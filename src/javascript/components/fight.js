import { controls } from '../../constants/controls';
import { getDOMElement, setStyleAttribute } from '../helpers/domHelper';

const TIME_REST_AFTER_CRIT_HIT = 10000;
const STR_FIGHTER_ONE = 'PlayerOne';
const STR_FIGHTER_TWO = 'PlayerTwo';
const STR_BLOCK = 'Block';
const STR_ATTACK = 'Attack';
const STR_CRIT_HIT = 'CriticalHitCombination';

const pressedKeys = new Set();
const combinationHits = [];

const deleteUnPressedKey = arrayKeys => {
  arrayKeys.forEach( key => pressedKeys.delete(key))
};

const addControls = e => {
  pressedKeys.add(e.code);
};

const getCombinations = code =>
  Object.keys(controls).filter(key => {
    const oneCombination = controls[key];
    if(Array.isArray(oneCombination)){
      return oneCombination.includes(code)
    }
    return code === oneCombination;
  });

const removeControls = e => {
  const {code: pressedKey} = e;
  if(!pressedKeys.has(pressedKey)){
    return ;
  }
  const pressedCombinations = getCombinations(pressedKey);
  const listKeys = [pressedKey];
  if(pressedCombinations.length){
    combinationHits.push(...pressedCombinations);

    pressedCombinations.forEach( key => {
      const combination = controls[key];
      Array.isArray(combination) ? listKeys.push(...combination) : listKeys.push(combination);
    });
  }
  deleteUnPressedKey(listKeys);
};

const showHealth = (healthBar, fighter, health) => {
  const { health: startHealth } = fighter;
  const percentHealth = (health / startHealth) * 100;
  const newValue = `${Math.max(0, percentHealth)}%`;
  setStyleAttribute(healthBar, 'width', newValue);
};

const arrayDifference = (arrayA, arrayB) => {
  arrayB.forEach( elementB =>
    arrayA = [].concat(...arrayA.filter(
      elementA => !elementA.includes(elementB.substr(0, 9))
    ))
  );
  return arrayA;
};

const arrayFilter = (arr, substr) => arr.filter( element => element.includes(substr));

const calculateHit = (healthDefender, actionAttacks, actionBlocks, attacker, defender, strConst) => {
  if (actionAttacks[0].substr(0, 9) === strConst) {
    healthDefender -= actionBlocks.length ? getDamage(attacker, defender) : getHitPower(attacker);
  }
  return healthDefender;
};

const calculateCriticalHit = (healthDefender, action, attacker, timeNow, timeAttacker, strConstr, timeConst ) => {
  if((action.substr(0, 9) === strConstr) && (timeNow - timeAttacker >= timeConst)){
    healthDefender -= getCriticalPower(attacker);
    timeAttacker = timeNow;
  }
  return [healthDefender, timeAttacker];
};


export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // + todo: implement this method
    // resolve the promise with the winner when fight is over

    const healthBarFirst = getDOMElement('#left-fighter-indicator');
    const healthBarSecond = getDOMElement('#right-fighter-indicator');

    let { health: healthFirst } = firstFighter;
    let { health: healthSecond } = secondFighter;
    let timeFirst = 0;
    let timeSecond = 0;

    const game = () => {
      // якщо робити нічого виходимо
      if(!combinationHits.length){
        requestAnimationFrame(game);
        return;
      }

      let timeNow = performance.now();
      let actions = [].concat(...combinationHits);
      combinationHits.length = 0;

      const actionsBlock =  arrayFilter(actions, STR_BLOCK);
      if(actionsBlock.length){
        actions = arrayDifference(actions, actionsBlock);
        actions.push(...actionsBlock);
      }

      // шукаємо чи є бійці з критичним ударом
      const actionCriticalHits = arrayFilter(actions, STR_CRIT_HIT);
      if(actionCriticalHits.length){
        actions = arrayDifference(actions, actionCriticalHits);
        actionCriticalHits.forEach( action => {

          [healthSecond, timeFirst ] = calculateCriticalHit(healthSecond, action, firstFighter, timeNow, timeFirst, STR_FIGHTER_ONE, TIME_REST_AFTER_CRIT_HIT);
          [healthFirst, timeSecond] = calculateCriticalHit(healthFirst, action, secondFighter, timeNow, timeSecond, STR_FIGHTER_TWO, TIME_REST_AFTER_CRIT_HIT);
        })
      }

      if(actions.length){
        const actionAttacks = arrayFilter(actions, STR_ATTACK);
        const actionBlocks = arrayFilter(actions, STR_BLOCK);

        if(actionAttacks.length) {
          healthSecond = calculateHit(healthSecond, actionAttacks, actionBlocks, firstFighter, secondFighter, STR_FIGHTER_ONE);
          healthFirst = calculateHit(healthFirst, actionAttacks, actionBlocks, firstFighter, secondFighter, STR_FIGHTER_TWO);
        }
      }

      showHealth(healthBarFirst, firstFighter, healthFirst);
      showHealth(healthBarSecond, secondFighter, healthSecond);

      if (healthFirst <= 0 || healthSecond <= 0) {
        pressedKeys.clear();
        document.removeEventListener('keydown', addControls);
        document.removeEventListener('keyup', removeControls);
        resolve( healthFirst <= 0 ? secondFighter : firstFighter);
      }

      requestAnimationFrame(game);
    };

    document.addEventListener('keydown', addControls);
    document.addEventListener('keyup', removeControls);
    requestAnimationFrame(game);
  });
}

export function getDamage(attacker, defender) {
  // + todo: implement this method
  // return damage
  let damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(damage, 0);
}

function getRandom(max) {
  return (max - 1) + Math.random();
}

export function getHitPower(fighter) {
  // + todo: implement this method
  // return hit power
  const criticalHitChance = getRandom(2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // + todo: implement this method
  // return block power
  const dodgeChance = getRandom(2);
  return fighter.defense * dodgeChance;
}

export function getCriticalPower(fighter) {
  return 2 * fighter.attack;
}
