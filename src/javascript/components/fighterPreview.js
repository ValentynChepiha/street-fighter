import { createElement } from '../helpers/domHelper';
import { characters } from '../../constants/characters';

function createFighterCharacters(fighter){
  const container = createElement({
    tagName: 'div',
    className: 'fighter-preview__info'
  });
  characters.forEach(element => {
    let character = createElement({
      tagName: 'p'
    });
    character.innerText = `${element}: ${fighter[element]}`;
    container.append(character);
  });
  return container;
}

function createFighterSmallImage(fighter, position){
  const transformClassName = position === 'right' ? 'fighter-preview___transform' : '';
  const fighterImg = createFighterImage(fighter);
  const container = createElement({
    tagName: 'div',
    className: `fighter-preview___img_small ${transformClassName}`,
  });
  container.append(fighterImg);
  return container;
}

export function createFighterPreview(fighter, position) {
  if(fighter === undefined){
    return '';
  }
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // + todo: show fighter info (image, name, health, etc.)

  const containerImage = createFighterSmallImage(fighter, position);
  const containerCharacters = createFighterCharacters(fighter);

  fighterElement.append(containerImage, containerCharacters);
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
