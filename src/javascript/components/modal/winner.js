import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // + todo: implement this method
  // call showModal function
  const title = 'Our winner!!!';
  const bodyElement = fighter.name;
  const onClose = () => { window.location.reload() };
  showModal({title, bodyElement, onClose});
}
